DEBUG = True

KUBERNETES_TOKEN_FILE = '/var/run/secrets/kubernetes.io/serviceaccount/namespace'


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s [%(levelname)s] %(name)s %(message)s'
        },
        'coloredlogs': {
            # https://github.com/xolox/python-coloredlogs/issues/40
            '()': 'coloredlogs.ColoredFormatter',
            'fmt': '%(asctime)s.%(msecs)03d %(levelname)s %(name)s %(message)s',
        },
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            # 'formatter': 'default',
            'formatter': 'coloredlogs',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stderr',
        },
        # Not available before Flask 0.13, but seems not needed
        # as it's sys.stderr anyway
        # 'wsgi': {
        #     'class': 'logging.StreamHandler',
        #     'stream': 'ext://flask.logging.wsgi_errors_stream',
        #     'formatter': 'default',
        # },
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': True
        },
        'parso': {
            'handlers': [],
            'propagate': False,
        },

        # 'sqlalchemy.engine': {
        #     'handlers': ['default'],
        #     'level': 'DEBUG',
        #     'propagate': False,
        # },
    }
}

GEVENT_LISTEN = '0.0.0.0:8088'

IPYTHON_CONFIG = {'InteractiveShellApp': {'exec_lines': [
    '%load_ext autoreload',
    '%autoreload 2',
    ('import logging; logging.basicConfig(level=logging.DEBUG, '
     'format="%(asctime)s.%(msecs)03d %(levelname)s %(name)s %(message)s")'),
]}}
