# Worker started by Worker Manager, get Github repo URL as parameter
# It expose API endpoint that say if Worker finished all jobs or not
# Worker download repo with init container, save it to PVC
# It checks which languages used (for test use Python/Java)
# Worker start Kubernetes jobs for each language with mounted volume with cloned repo(Validator Engines)
# Worker monitors Validator Engines logs, check if JSON returned. IF not returned, VE should be have timeout.

# Init container:
# Create PVC to store repository
# Clone repository(URL as ENV variable)

import gevent.monkey
import random
import string
import os
gevent.monkey.patch_all()  # noqa

from vm import ValidatorManager
from flask_vgavro_utils.gevent import GeventFlask, serve_forever
from flask_restful import Resource, Api

PVC = os.environ.get('PVC_NAME')
GIT_CLONE_REPO = os.environ.get('REPOSITORY_URL')
WORKER_NAMESPACE = os.environ.get('WORKER_NAMESPACE')

JAVA_IMAGE = "gcr.io/k8s-fourcast/java-validator:517bc42bc37a65580d5593b399775a45dfa892b4"
PYTHON_IMAGE = "docker pull gcr.io/k8s-fourcast/python-validator:949316d61266f79e0133560f14c22aadac9347ee"

app = GeventFlask(__name__)
app.config.from_object('config')
#app.config.from_object('config_local', raise_no_module=False)
api = Api(app)
app.configure()

vm = ValidatorManager(app.spawn)

def generate_session_id():
    return ''.join([random.choice(string.ascii_letters + string.digits)
            for n in range(10)])

class Status(Resource):
    def get(self):
        for instance in vm.instances.copy().values():
            log = instance.check_logs()
            print(log)
        return {'status': 'FAILED'}

def main():
    # Kinda checking what languages we have in the repo(detects Python/Java)
    languages = [("python", PYTHON_IMAGE), ("java", JAVA_IMAGE)]
    for item in languages:
        session_id = generate_session_id()
        language = item[0]
        image = item[1]
        vm.create_validator(language, session_id, PVC, GIT_CLONE_REPO, WORKER_NAMESPACE, image)
    serve_forever(app)

api.add_resource(Status, '/status')

if __name__ == "__main__":
    main()