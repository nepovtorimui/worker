# Used to store Worker instances and their metadata
import kubernetes
import time

class WorkerManager:
    def __init__(self, spawn):
        self.spawn = spawn
        self.session_id = ''
        self.instances = {}

        kubernetes.config.load_incluster_config()
        self.client = kubernetes.client.CoreV1Api()

    def _create_namespace(self, name):
        self.client.create_namespace(kubernetes.client.V1Namespace(metadata=kubernetes.client.V1ObjectMeta(name=name)))
        time.sleep(5)

    def _create_pvc_object(self, name):
        body = kubernetes.client.V1PersistentVolumeClaim()
        metadata = kubernetes.client.V1ObjectMeta()
        metadata.name = "pvc-{}".format(name.lower())
        body.metadata = metadata
        spec = kubernetes.client.V1PersistentVolumeClaimSpec()
        spec.access_modes = ["ReadWriteOnce"]
        resources = kubernetes.client.V1ResourceRequirements()
        resources.requests = {"storage": "10G"}
        spec.resources = resources
        body.spec = spec

        return body

    def _create_pvc(self, pvc):
        self.client.create_namespaced_persistent_volume_claim(self.namespace, pvc)

    def _create_pv_object(self, name):
        body = kubernetes.client.V1PersistentVolume()
        metadata = kubernetes.client.V1ObjectMeta()
        metadata.name = "pv-{}".format(name.lower())
        body.metadata = metadata
        spec = kubernetes.client.V1PersistentVolumeSpec()
        spec.access_modes = ["ReadWriteOnce"]
        spec.capacity = {"storage": "100G"}
        gce_persistent_disk = kubernetes.client.V1GCEPersistentDiskVolumeSource(pd_name='poc-1')
        gce_persistent_disk.fs_type = 'ext4'
        gce_persistent_disk.pd_name = 'poc-1'
        spec.gce_persistent_disk = gce_persistent_disk
        spec.persistent_volume_reclaim_policy = "Retain"
        body.spec = spec

        return body

    def _create_pv(self, pv):
        self.client.create_persistent_volume(pv)

    def _create_service_object(self, session_id):
        body = kubernetes.client.V1Service()
        metadata = kubernetes.client.V1ObjectMeta()
        metadata.name = "service-worker-{}".format(session_id.lower())
        body.metadata = metadata
        spec = kubernetes.client.V1ServiceSpec()
        port = kubernetes.client.V1ServicePort(port=80)
        port.protocol = 'TCP'
        port.target_port = 3000
        port.port = 80
        spec.ports = [port]
        spec.selector = {"session_id": session_id}
        body.spec = spec

        return body

    def _create_deployment_object(self, session_id):
        self.pod_name = 'worker-{}'.format(session_id.lower())
        # Configureate Pod template container
        mount = kubernetes.client.V1VolumeMount(
            mount_path="/tmp/worker",
            name="worker-volume-{}".format(session_id.lower()),
            sub_path=session_id.lower()
        )

        container = kubernetes.client.V1Container(
            name="worker-{}".format(session_id.lower()),
            image='nginx:latest',
            #command=self.conf[language].get('command'),
            tty=True,
            stdin=True,
            ports=[kubernetes.client.V1ContainerPort(container_port=3000)],
            volume_mounts=[mount]
        )
        # Create and configurate a spec section
        labels = {
            'session_id': session_id
        }

        pvc = kubernetes.client.V1PersistentVolumeClaimVolumeSource(
            claim_name="pvc-{}".format(session_id.lower()),
            read_only=False
        )

        volume = kubernetes.client.V1Volume(
            name="worker-volume-{}".format(session_id.lower()),
            persistent_volume_claim=pvc
        )

        pod = kubernetes.client.V1Pod(
            metadata=kubernetes.client.V1ObjectMeta(name=self.pod_name, labels=labels),
            spec=kubernetes.client.V1PodSpec(containers=[container], volumes=[volume])
            #spec=kubernetes.client.V1PodSpec(containers=[container])
        )

        return pod

    def _create_deployment(self, deployment):
        # Create deployment
        api_response = self.client.create_namespaced_pod(
            body=deployment,
            namespace=self.namespace)
        return api_response

    def _create_service(self, service):
        api_response = self.client.create_namespaced_service(self.namespace, service)
        return api_response

    def pod_deploy(self, session_id, namespace):
        self.namespace = namespace.lower()
        #pv = self._create_pv_object(session_id)
        #self._create_pv(pv)
        self._create_namespace(self.namespace)
        pvc = self._create_pvc_object(session_id)
        self._create_pvc(pvc)
        deployment = self._create_deployment_object(session_id)
        service = self._create_service_object(session_id)
        resp = self._create_deployment(deployment)
        service_response = self._create_service(service)
        # That's blocking, is there a way to use greenlet?
        self.instances[session_id].initialized = True
        for retry in range(300):
            status = self.get_pod_status(session_id)
            if status in 'Running':
                break
            time.sleep(1)
        else:
            raise RuntimeError('Retries exceeded')

    def get_pod_info(self, session_id):
        # Add handling exceptions, when pod is not found
        label_selector = 'session_id={}'.format(session_id)
        pod_object = self.client.list_namespaced_pod(self.namespace, label_selector=label_selector)
        return pod_object.items[0].metadata.name, pod_object.items[0].metadata.labels

    def get_pod_status(self, session_id):
        pod_name, labels = self.get_pod_info(session_id)
        phase = self.client.read_namespaced_pod(pod_name, self.namespace).status.phase
        return phase

    def delete_worker(self, namespace):
        session_id = namespace
        body = kubernetes.client.V1DeleteOptions()
        api_response = self.client.delete_namespace(namespace.lower(), body)
        del self.instances[session_id]

    def create_worker(self, git_url, session_id):
        self.spawn(self.pod_deploy, session_id, session_id)
        self.instances[session_id] = Worker(git_url, session_id)

class Worker:
    def __init__(self, git_url, namespace):
        self.namespace = namespace.lower()
        self.git_url = git_url
        self.initialized = False

    def get_status(self):
        return "{} HTTP STATUS: FAILED".format(self.namespace)
