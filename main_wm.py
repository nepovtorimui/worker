# Worker manager checks for database records over period of time (1 minute)
# When it see new records, it create Worker for each record
# After WM started processing record it should somehow indicate that this record in progress
# Checks Workers with http call to know it they finished processing

import gevent.monkey
gevent.monkey.patch_all()  # noqa

import random
import string
import datetime
import time

from models import RepoData
from wm import WorkerManager
from flask_vgavro_utils.gevent import GeventFlask, serve_forever

app = GeventFlask(__name__)
app.configure()

wm = WorkerManager(app.spawn)


def generate_session_id():
    return ''.join([random.choice(string.ascii_letters + string.digits)
            for n in range(10)])


def get_new_records():
    query = RepoData.select().where(RepoData.start_time == None)
    return query


def worker(git_url):
    sessiod_id = generate_session_id()
    wm.create_worker(git_url, sessiod_id)


@app.work_forever(60)
def create_worker():
    records = get_new_records()
    if len(records) > 0:
        for record in records:
            worker(record.git_repo_url)
            record.start_time = datetime.datetime.now()
            record.save()


@app.work_forever(10)
def check_worker():
    for name, instance in wm.instances.copy().items():
        if "OK" in instance.get_status():
            wm.delete_worker(name)
            print("Worker {} deleted".format(name))


def main():
    serve_forever(app)


if __name__ == "__main__":
    main()