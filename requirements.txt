Flask==1.0.2
Flask-RESTful==0.3.6
webargs
peewee
pyyaml
kubernetes==8.0.0
-e git+https://github.com/vgavro/flask-vgavro-utils#egg=flask-vgavro-utils
gevent
-e ./

# dev
ipython
pdbpp
coloredlogs
pytest
flake8
