#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='worker',
    version='0.0.1',
    description='https://gitlab.com/nepovtorimui/worker',
    author='Stasik & Vitya International (TM)',
    url='https://gitlab.com/nepovtorimui/worker',
    keywords='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[],
    tests_require=[],
    entry_points={
        'console_scripts': [
            'worker=main:main',
        ],
    },
)
