FROM python:3

LABEL author="Rybak Stanislav"
COPY main.py vm.py requirements.txt setup.cfg setup.py /app/worker/
WORKDIR /app/worker
RUN apt-get update && apt-get install -y git
RUN pip3 install -r requirements.txt

ENTRYPOINT [ "worker" ]

