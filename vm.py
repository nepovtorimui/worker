import time
import kubernetes

class ValidatorManager:
    def __init__(self, spawn):
        self.spawn = spawn
        self.instances = {}

        kubernetes.config.load_incluster_config()
        self.client = kubernetes.client.CoreV1Api()

    def _create_deployment(self, deployment):
        # Create deployment
        api_response = self.client.create_namespaced_pod(
            body=deployment,
            namespace=self.namespace)
        return api_response

    def pod_deploy(self, language, pvc_name, git_repo, namespace, session_id, image):
        self.namespace = namespace
        deployment = self._create_deployment_object(language, session_id, pvc_name, git_repo, image)
        resp = self._create_deployment(deployment)
        for retry in range(300):
            status = self.get_pod_status(session_id)
            if status in 'Running':
                break
            time.sleep(1)
        else:
            raise RuntimeError('Retries exceeded')

    def get_pod_info(self, session_id):
        # Add handling exceptions, when pod is not found
        label_selector = 'session_id={}'.format(session_id)
        pod_object = self.client.list_namespaced_pod(self.namespace, label_selector=label_selector)
        return pod_object.items[0].metadata.name, pod_object.items[0].metadata.labels

    def get_pod_status(self, session_id):
        pod_name, labels = self.get_pod_info(session_id)
        phase = self.client.read_namespaced_pod(pod_name, self.namespace).status.phase
        return phase

    def create_validator(self, language, session_id, pvc_name, git_repo, namespace, image):
        self.spawn(self.pod_deploy, language, pvc_name, git_repo, namespace, session_id, image)
        self.instances[session_id] = Validator(language, namespace, session_id)

    def _create_deployment_object(self, language, session_id, pvc_name, git_repo, image):
        self.pod_name = 'validator-{}-{}'.format(language, session_id.lower())
        # Configureate Pod template container
        mount = kubernetes.client.V1VolumeMount(
            mount_path="/mnt",
            name="worker-volume-{}".format(session_id.lower()),
            sub_path="git"
        )

        git_repo = kubernetes.client.V1EnvVar(name="GIT_CLONE_REPO", value=git_repo)
        git_branch = kubernetes.client.V1EnvVar(name="GIT_CLONE_BRANCH", value="master")
        git_dest = kubernetes.client.V1EnvVar(name="GIT_CLONE_DEST", value="/tmp/")
        env_namespace = kubernetes.client.V1EnvVar(name="WORKER_NAMESPACE", value=self.namespace)
        env = [git_repo, git_branch, git_dest, env_namespace]

        init = kubernetes.client.V1Container(name="init-container", env=env, image="emblica/git-cloner")

        container = kubernetes.client.V1Container(
            name="validator-{}-{}".format(language, session_id.lower()),
            image='{}'.format(image),
            #command=self.conf[language].get('command'),
            env=env,
            tty=True,
            stdin=True,
            ports=[kubernetes.client.V1ContainerPort(container_port=3000)],
            volume_mounts=[mount]
        )
        # Create and configurate a spec section
        labels = {
            'session_id': session_id
        }

        pvc = kubernetes.client.V1PersistentVolumeClaimVolumeSource(
            claim_name=pvc_name,
            read_only=False
        )

        volume = kubernetes.client.V1Volume(
            name="worker-volume-{}".format(session_id.lower()),
            persistent_volume_claim=pvc
        )

        pod = kubernetes.client.V1Pod(
            metadata=kubernetes.client.V1ObjectMeta(name=self.pod_name, labels=labels),
            spec=kubernetes.client.V1PodSpec(containers=[container], init_containers = [init], volumes=[volume])
        )

        return pod

class Validator():
    def __init__(self, language, namespace, session_id):
        self.language = language
        self.namespace = namespace
        self.session_id = session_id

        kubernetes.config.load_incluster_config()
        self.client = kubernetes.client.CoreV1Api()

    def check_logs(self):
        name = self.get_name()
        logs = self.client.read_namespaced_pod_log(name, self.namespace)
        return logs

    def get_name(self):
        # Add handling exceptions, when pod is not found
        label_selector = 'session_id={}'.format(self.session_id)
        pod_object = self.client.list_namespaced_pod(self.namespace, label_selector=label_selector)
        return pod_object.items[0].metadata.name